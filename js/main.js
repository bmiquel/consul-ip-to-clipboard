const COPY_TO_CLIPBOARD_IMG_URL = "https://www.shareicon.net/data/16x16/2015/12/18/689216_clipboard_512x512.png"
const COPY_TO_CLIPBOARD_IMG_SIZE = 25
const COPY_TO_CLIPBOARD_IMG_SIZE_ON_CLICK = 20
const COPY_TO_CLIPBOARD_FEEDBACK_MESSAGE_MS = 1000

function updatePanelCopyToClipboard(panel){
	var ipElement = panel.find('small')
	var ip = ipElement.text()
	panel.find('.copy-to-clipboard').each(function(){$(this).remove()})
	var $copyToClipboard = $('<button class="copy-to-clipboard">'
		+ '<img title="Copy to clipboard" alt="Copy to clipboard"'
		+' src="' + COPY_TO_CLIPBOARD_IMG_URL + '"'
		+ 'width="' + COPY_TO_CLIPBOARD_IMG_SIZE 
		+ '" height="' + COPY_TO_CLIPBOARD_IMG_SIZE + '">'
		+ '<span hidden class="copy-tooltip">&nbsp;&nbsp;Copied!</span>'
		+ '</button>')
	panel.append($copyToClipboard)
	$copyToClipboard.click(function(event){
		var textArea = document.createElement("textarea");
		$(textArea).attr('hidden')
		textArea.value = ip
		document.body.appendChild(textArea)
		var previousScroll = window.pageYOffset
		textArea.focus()
		textArea.select()
		document.execCommand("copy")
		var feedbackSpan = $copyToClipboard.children('.copy-tooltip')[0]
		$(feedbackSpan).show().delay(COPY_TO_CLIPBOARD_FEEDBACK_MESSAGE_MS).fadeOut()
		event.stopPropagation()
		window.scrollTo(0, previousScroll)
		document.body.removeChild(textArea)
	})
	$copyToClipboard.mouseup(function(){
		var $image = $($copyToClipboard.children('img')[0])
		$image.width(COPY_TO_CLIPBOARD_IMG_SIZE)
		$image.height(COPY_TO_CLIPBOARD_IMG_SIZE)
	})
	$copyToClipboard.mousedown(function(){
		var $image = $($copyToClipboard.children('img')[0])
		$image.width(COPY_TO_CLIPBOARD_IMG_SIZE_ON_CLICK)
		$image.height(COPY_TO_CLIPBOARD_IMG_SIZE_ON_CLICK)
	})
}

new MutationObserver(function(mutationList) {
	for (var mutation of mutationList) {
		for (var child of mutation.addedNodes) {
			var $child = $(child)
			$child.find('.panel-title').each(function(){
				updatePanelCopyToClipboard($(this))
			})
		}
	}
}).observe(document.body, {
	attributes: true,
	childList: true,
	characterData: true,
	subtree: true
})