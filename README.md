# Consul IP to clipboard

Allow displayed IP directions at Consul to be copied to clipboard

## Installing

### Firefox

1. https://addons.mozilla.org/es/firefox/addon/consul-to-clipboard/

### Google Chrome ([not published because it costs 5$](https://developer.chrome.com/webstore/publish#pay-the-developer-signup-fee))

1. Clone or download [this](https://gitlab.com/bmiquel/consul-ip-to-clipboard) repository
2. Open Google Chrome, create a new tab and **type**: chrome://extensions (this hyperlink won't work due to security reasons)
3. Enable developer mode
4. Click on **load unpacked** button and select the folder created by cloning project at first step

## Considerations

Please note this is an amateur work developed at free time and I am aware that logo, code or UX could be improved and I might do it in the future.
Feel free to contribute.